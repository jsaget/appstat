\documentclass[a4paper,10pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}

\usepackage[hidelinks]{hyperref}
\usepackage{graphicx}

\usepackage[top=1cm,bottom=2cm,left=1cm,right=1cm]{geometry}

\usepackage{amsmath,amssymb}
\usepackage{dsfont}

\usepackage{multirow}

\usepackage{float}

\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}

\title{\textsc{Introduction à\\l'Apprentissage Statistique}\\Devoir Maison 2}
\author{Jules Saget}
\date{21 mai 2019}

\begin{document}

\maketitle

\begin{enumerate}
  \item
    \begin{enumerate}
      \item Il est souvent important de régulariser, car assurer une petite
        valeur de $\hat{\beta}$ permet de limiter le surapprentissage.
        
      \item Les erreurs sont représentée en figure \ref{fig:1b}

        \begin{figure}[t]
          \centering
          \includegraphics[width=0.4\textwidth]{1b}
          \caption{Erreurs d'entraînement et de test pour la descente de
            gradient stochastique associée à la régression linéaire
            régularisée par la norme 1 en fonction de $\lambda$\label{fig:1b}}
        \end{figure}

      \item La bonne valeur de $\lambda$ est celle qui minimise l'erreur de
        test.
        Toutefois, pour éviter les biais, on ne peut pas se contenter de
        regarder le $\lambda$ qui minimise l'erreur dans ce cas précis.
        On pourrait donc effectuer une validation croisée.
        En effectuant cette validation avec un découpage en 5 ensembles,
        j'obtiens $\hat{\lambda} = 0$ pour le meilleur $\lambda$.
        Ce résultat est plutôt étonnant.

      \item Les images sont représentées pour des $\lambda$ choisis à chaque
        tiers\footnote{le meilleur, puis celui tel que $1/3$ des valeurs soient
        meilleures, puis celui tel que $2/3$ des valeurs soient meilleurs, puis
        le pire.}.
        Elles sont en figure \ref{fig:1d}.

        \begin{figure}[t]
          \centering
          \includegraphics[width=0.4\textwidth]{1d}

          \caption{Représentations de l'estimateur pour différentes valeurs de
          $\lambda$\label{fig:1d}}
        \end{figure}
        
      \item Le résultat est assez similaire.
        Les erreurs sont représentée en figure \ref{fig:1e}.

        \begin{figure}[t]
          \centering
          \includegraphics[width=0.4\textwidth]{1e}
          \caption{Erreurs d'entraînement et de test pour la descente de
            gradient stochastique associée à la régression linéaire
            régularisée par la norme 2 en fonction de $\lambda$\label{fig:1e}}
        \end{figure}

    \end{enumerate}

  \item
    \begin{enumerate}
      \item
        Il suffit de faire le calcul.
        Soit $0 < i \leq n$, supposons $Y_i = 1$~:

        \begin{align*}
          \mathds{P}(Y = 1 | X = X_i)
          &= \mathds{P}(X = X_i | Y = 1)
          \frac{
            \mathds{P}(Y = 1)
          }{
            \mathds{P}(X = X_i | Y = 1)  \mathds{P}(Y = 1)
            \mathds{P}(X = X_i | Y = -1) \mathds{P}(Y = -1)
          }
          \\
          &= \frac{
            \pi e^{
              - \frac{1}{2} (X_i-\mu_1)^\top \Sigma^{-1} (X_i-\mu_1)
            }
          }{
            \pi e^{
              - \frac{1}{2} (X_i-\mu_1)^\top \Sigma^{-1} (X_i-\mu_1)
            } +
            (1 - \pi) e^{
              - \frac{1}{2} (X_i-\mu_{-1})^\top \Sigma^{-1} (X_i-\mu_{-1})
            }
          }
          \\
          &= \frac{1}{
            1 + e^{
              - X_i^\top \Sigma^{-1} (\mu_1 - \mu_{-1})
              - \frac{1}{2} (
                {\mu_{-1}}^\top \Sigma^{-1} \mu_{-1} -
                {\mu_1}^\top \Sigma^{-1} \mu_1
              ) - \log\left( \frac{\pi}{1-\pi} \right) 
            }
          }
        \end{align*}

        Le calcul avec $Y_i = -1$ s'effectue de manière similaire.
        On en déduit~:

        \begin{equation*}
          \boxed{
            \mathds{P}(Y = Y_i | X = X_i) =
            \frac{1}{
              1 + e^{
                - Y_i  X_i^\top \Sigma^{-1} (\mu_1 - \mu_{-1})
                - Y_i \frac{1}{2} (
                  {\mu_{-1}}^\top \Sigma^{-1} \mu_{-1} -
                  {\mu_1}^\top \Sigma^{-1} \mu_1
                ) - Y_i \log\left( \frac{\pi}{1-\pi} \right) 
              }
            }
          }
        \end{equation*}

      \item On va montrer que ce modèle ce ramène à un problème de régression
        logistique affine.
        On souhaite maximiser la probabilté de trouver la bonne réponse pour
        chaque entrée.
        On veut trouver $\hat{\Sigma}, \hat{\mu_1}, \hat{\mu_{-1}}, \hat{\pi}$
        tels que~:
      
        \begin{align*}
          \hat{\Sigma}, \hat{\mu_1}, \hat{\mu_{-1}}, \hat{\pi}
          &= \argmax_{
            \substack{
              \Sigma \in \mathcal{S}_n^{++}\\
              \mu_1, \mu_{-1} \in \mathds{R}^n\\
              \pi \in [0, 1]
            }
          } \prod_{i=1}^n \mathds{P}(Y = Y_i | X = X_i)
          \\
          &= \argmin \sum_{i=1}^n \log \left(
            \frac{1}{\mathds{P}(Y = Y_i | X = X_i)}
          \right)
          \\
          &= \argmin \sum_{i=1}^n \log \left(
            1 + e^{
              - Y_i  X_i^\top \Sigma^{-1} (\mu_1 - \mu_{-1})
              - Y_i \frac{1}{2} (
                {\mu_{-1}}^\top \Sigma^{-1} \mu_{-1} -
                {\mu_1}^\top \Sigma^{-1} \mu_1
              ) - Y_i \log\left( \frac{\pi}{1-\pi} \right) 
            }
          \right) 
        \end{align*}

  \item Dans cette question on considère le modèle génératif.
    On veut maximiser $L$ la vraisemblance (logarithmique) des données, donné
    par~:
    
    \begin{align*}
      L(X, Y; \pi, \mu, \Sigma)
      &= \log \left(  
        \prod_{i=1}^n \mathds{P} \left( X=X_i,Y=Y_i  \right)
      \right)
      \\
      &= \sum_{i=1}^n {
        \log \left(
          \mathds{P} \left( Y=Y_i \right)
          \mathds{P} \left( X=X_i | Y=Y_i \right)
        \right)
      }
    \end{align*}
    où $\mu = (\mu_1, \mu_{-1})$.

    Soit $i$ tel que $Y_i = 1$~:
    
    \begin{align*}
      \mathds{P} \left( Y=1 \right)
      \mathds{P} \left( X=X_i | Y=1 \right)
      &= \pi
      \frac{1}{
        \sqrt{(2 \pi)^N \det \Sigma}
      }
      e^{-\frac{1}{2} (X_i-\mu_{Y_i})^\top \Sigma^{-1} (X_i-\mu_{Y_i})}
    \end{align*}

    Pour la suite, je note $\pi_i = \mathds{P}\left( Y = Y_i \right)$.

    On obtient donc~:

    \begin{align*}
      L(X, Y; \pi, \mu, \Sigma)
      &= \sum_{i=1}^n \left(
        \log \pi_i
        - \frac{1}{2} \log \left( 
          (2 \pi)^N \det \Sigma
        \right)
      \right)
      +
      \sum_{i=1}^n {
        - \frac{1}{2} (X_i - \mu_{Y_i})^\top \Sigma^{-1} (X_i - \mu_{Y_i})
      }
    \end{align*}

    On veut calculer les gradients dans les différentes directions.
    Je note $\nabla_\Sigma L$ le gradient de la fonction $\Sigma \mapsto
    L\left(X, Y; \pi, (\mu_1, \mu_{-1}), \Sigma \right)$ (en supposant les
    autres variables fixées), et ainsi de suite pour toutes les entrées de $L$~:
        \begin{align*}
          \nabla_\Sigma L
          &=
          \sum_{i=1}^n \left(
            - \frac{1}{2} \left(
              \Sigma^{-1}
              - \Sigma^{-1} (X_i - \mu_{Y_i}) (X_i - \mu_{Y_i})^\top \Sigma^{-1}
            \right)
          \right)
        \end{align*}

        \begin{align*}
          \nabla_{\mu_1} L
          &= \sum_{\substack{i=1\\Y_i = 1}}^n {
            \Sigma^{-0,5} (X_i - \mu_1)
          }
          &
          \nabla_{\mu_{-1}} L
          &= \sum_{\substack{i=1\\Y_i = -1}}^n {
            \Sigma^{-0,5} (X_i - \mu_{-1})
          }
          &
          \nabla_\pi L
          &= \sum_{i=1}^n {
            \frac{Y_i}{\pi_i}
          }
        \end{align*}

        On cherche maintenant à annuler chacun de ces gradients.
        On a ainsi~:
        \begin{align*}
          \hat{\Sigma}   &= \frac{1}{n} \sum_{i=1}^n {
            (X_i - \mu_{Y_i}) (X_i - \mu_{Y_i})^\top
          }
          &
          \hat{\mu_1}    &= \frac{\pi}{n} \sum_{\substack{i=1\\Y_i=1}}^n {
            X_i
          }
          &
          \hat{\mu_{-1}}
          &= \frac{(1 - \pi)}{n} \sum_{\substack{i=1\\Y_i=-1}}^n {
            X_i
          }
          &
          \hat{\pi}      &= \frac{1}{3}
        \end{align*}


      \item Un faux positif est une lettre identifiée comme A par l'algorithme,
        mais qui est en fait un B ou un C.
        À l'inverse, un faux négatif est une lettre identifiée comme B ou C par
        l'algorithme, mais qui est en fait un A.
        La matrice de confusion associée à LDA sur l'ensemble de test est la
        suivante~:

        \begin{table}[H]
          \centering
          \begin{tabular}{ccc|c}
            & & \multicolumn{2}{c}{Prédiction} \\
            & \multicolumn{1}{c|}{} & A & B ou C \\
            \cline{2-4}
            \multirow{2}{*}{\rotatebox[origin=c]{90}{Valeur}}
            & \multicolumn{1}{c|}{A} & 206 & 44 \\
            \cline{2-4}
            & \multicolumn{1}{c|}{B ou C} & 105 & 295
          \end{tabular}
        \end{table}
        
        Considérer la matrice de confusion permet de vérifier indépendemment la
        sensibilité et la spécificité d'un test.
        Ici, l'algorithme est plutôt sensible, car il ne loupe qu'un A sur cinq
        (ce qui reste tout de même une erreur elevée).
        Toutefois, il est peu spécifique, vu qu'il identifie un B ou C sur trois
        comme étant un A.
        Ainsi, vu que les B et C sont plus nombreux, on a une chance sur trois
        d'avoir un A lorsque l'algorithme annonce A.

        Vu que le résultat est parfait sur l'ensemble d'entraînement, on peut
        supposer un fort surapprentissage.

    \end{enumerate}

  \item 
    \begin{enumerate}
      \item La résultat est visible en figure \ref{fig:3a}.
        L'identification des groupes dans la légende a été effectuée lors de la
        question suivante.

        \begin{figure}[t]
          \centering
          \includegraphics[width=0.4\textwidth]{3a}
          \caption{Représentation des groupes de lettres (projection sur les deux
          composantes principales avec PCA)\label{fig:3a}}
        \end{figure}

      \item La matrice de confusion est la suivante~:

        \begin{table}[H]
          \centering
          \begin{tabular}{ccc|c|c}
            & & \multicolumn{3}{c}{Prédiction} \\
            & \multicolumn{1}{c|}{} & A & B & C \\
            \cline{2-5}
            \multirow{3}{*}{\rotatebox[origin=c]{90}{Valeur}}
            & \multicolumn{1}{c|}{A} & 205 & 61 & 34 \\
            \cline{2-5}
            & \multicolumn{1}{c|}{B} & 28 & 211 & 61 \\
            \cline{2-5}
            & \multicolumn{1}{c|}{C} & 4 & 42 & 254
          \end{tabular}
        \end{table}


        Les résultats sont plutôt décevants.
        L'algorithme s'est trompé sur environ une valeur sur trois, et a tendance à
        beaucoup parier sur la lettre C.

        Cela indique sans doute que les groupes ne sont pas ronds avec le même
        diamètre~: l'algorithme des $k$-moyennes n'est sans doute pas le plus
        adapté à ce problème.
        
    \end{enumerate}

\end{enumerate}

\end{document}
